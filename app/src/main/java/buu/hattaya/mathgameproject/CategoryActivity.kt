package buu.hattaya.mathgameproject

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class CategoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)

        var sign: Int = 0
        var onClick: Int = 0

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener{
            sign = 1
            onClick = 1
        }

        val btnSubtract = findViewById<Button>(R.id.btnSubtract)
        btnSubtract.setOnClickListener{
            sign = 2
            onClick = 1
        }

        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener{
            sign = 3
            onClick = 1
        }

        val btnAll = findViewById<Button>(R.id.btnAll)
        btnAll.setOnClickListener{
            sign = 4
            onClick = 1
        }

        val btnStart = findViewById<Button>(R.id.btnStart)
        btnStart.setOnClickListener{
            if(onClick == 0) {
                Toast.makeText(CategoryActivity@this, "Please select a category.",Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent(CategoryActivity@ this, PlayGroundActivity::class.java)
                intent.putExtra("sign", sign.toString())
                startActivity(intent)
            }
        }

        val correct:String = intent.getStringExtra("correct")?:"0"
        val txtCorrect = findViewById<TextView>(R.id.txtCorrect)
        txtCorrect.text = correct

        val incorrect:String = intent.getStringExtra("incorrect")?:"0"
        val txtIncorrect = findViewById<TextView>(R.id.txtIncorrect)
        txtIncorrect.text = incorrect

    }
}