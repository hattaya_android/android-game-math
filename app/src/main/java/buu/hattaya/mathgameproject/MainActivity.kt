package buu.hattaya.mathgameproject

import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnEnterToGame = findViewById<Button>(R.id.btnEnterGame)
        btnEnterToGame.setOnClickListener{
            val intent = Intent(MainActivity@this, CategoryActivity:: class.java)
            startActivity(intent)
        }
        var soundBG = MediaPlayer.create(this, R.raw.effect)
        soundBG.start()
        soundBG.setLooping(true)


    }
}